build: unifont.bgf
# unifont_jp.bgf

VPATH=/usr/share/unifont

%.bdf: %.hex
	hex2bdf < $< > $@

%.bgf: %.bdf
	bdftobogl -b $< > $@

clean:
	rm -f *.bdf *.bgf
